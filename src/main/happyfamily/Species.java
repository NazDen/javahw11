package happyfamily;

public enum Species {

    DOG(false,4,true),DOMESTICCAT(false,4,true),ROBOCAT(false,4,true),FISH(false,0,false),UNKNOWN();

   private final boolean canfly;

   private final int numberOfLegs;

   private final boolean hasFur;

    Species(boolean canFly, int numberOfLegs,boolean hasFur) {
        this.canfly= canFly;
        this.numberOfLegs= numberOfLegs;
        this.hasFur= hasFur;
    }
    Species() {
        this.canfly= false;
        this.numberOfLegs= 0;
        this.hasFur= false;
    }

    public boolean isCanfly() {
        return canfly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean isHasFur() {
        return hasFur;
    }

    @Override
    public String toString() {
        return super.toString()+"{" +
                "canfly=" + canfly +
                ", numberOfLegs=" + numberOfLegs +
                ", hasFur=" + hasFur +
                '}';
    }
}
