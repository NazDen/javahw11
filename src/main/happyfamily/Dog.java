package happyfamily;

import java.util.HashSet;

public class Dog extends Pet implements Foul {
    @Override
    void respond() {
        System.out.println("Гав.");
    }

    @Override
    public void foul() {
        {
            System.out.println("Нужно хорошо замести следы...");
        }
    }

    public Dog(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.DOG);
    }
}
